#ifndef AES128DEF_H
#define AES128DEF_H

#define AES128_FPGA_ADDRESS     (0x90000)
#define AES128_FPGA_HWFID       (0xAE)  // FIXME: what value should be here?

#define AES128_INPUT_DATA       (0x0000)
#define AES128_INPUT_KEY        (0x0010)
#define AES128_INPUT_IV         (0x0020)
#define AES128_INPUT_TRIGGER    (0x00FF)

#define AES128_OUTPUT_DATA      (0x0100)
#define AES128_OUTPUT_STATE     (0x01ff)

#define AES128_STATE_BIT_READY  (0x01 << 0)

#endif // AES128DEF_H
