#ifndef AES128_H
#define AES128_H

#include <stddef.h>
#include <stdint.h>

#define AES128_BLOCK_SIZE 16 // = (128 bit / 8 bit)

struct Aes128
{
    uintptr_t userlogicOffset;
};

struct InitializationVector
{
    uint8_t iv[AES128_BLOCK_SIZE];
};
struct Key
{
    uint8_t key[AES128_BLOCK_SIZE];
};

int aes128_configureFpga(struct Aes128 *thiz);
void aes128_initValues(struct Aes128 *thiz, struct Key *key, struct InitializationVector *iv);

/* init: shotcut calling configureFpga and initValues */
int aes128_init(struct Aes128 *thiz, struct Key *key, struct InitializationVector *iv);
/* init2: for internal usage only! */
void aes128_init2(struct Aes128 *thiz, uintptr_t offset, struct Key *key,
                  struct InitializationVector *iv);
void aes128_encrypt(struct Aes128 *thiz, const uint8_t *data, uint8_t *result, size_t size);

/* for fine encryption control. maybe someone want to encrypt single blocks ¯\_(ツ)_/¯ */
struct Block
{
    struct Aes128 *aes128;
    uint8_t count;
    uint8_t data[AES128_BLOCK_SIZE];
};
void aes128_block_init(struct Block *block, struct Aes128 *aes128);
void aes128_block_encrypt(struct Block *block, struct Block *result);

#endif // AES128_H
