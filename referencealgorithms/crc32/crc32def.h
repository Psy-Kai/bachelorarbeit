#ifndef CRC32DEF_H
#define CRC32DEF_H

#define CRC32_FPGA_ADDRESS      (0x180000)  // FIXME: what value should be here?
#define CRC32_FPGA_HWFID        (0xC3)  // FIXME: what value should be here?

#define CRC32_INPUT_DATA        (0x0000)
#define CRC32_INPUT_TRIGGER     (0x00FF)

#define CRC32_OUTPUT_DATA       (0x0100)
#define CRC32_OUTPUT_STATE      (0x01FF)

#define CRC32_STATE_BIT_READY   (0x01 << 0)

#endif // CRC32DEF_H
