#ifndef CRC32_H
#define CRC32_H

#include <stddef.h>
#include <stdint.h>

struct Crc32
{
    uintptr_t userlogicOffset;
};

int crc32_configureFpga(struct Crc32 *thiz);

int crc32_init(struct Crc32 *thiz);
/* init2: for internal usage only! */
void crc32_init2(struct Crc32 *thiz, uintptr_t offset);

struct Crc32Data
{
    size_t count;
    uint8_t *data;
};
uint32_t crc32_calc(struct Crc32 *thiz, struct Crc32Data *crc32Data);

#endif // CRC32_H
