#define UNITY_INCLUDE_SETUP_STUBS
#include <unity.h>
#include <aes128.h>
#include <aes128def.h>

#define TEST_DATA_SIZE 128

void init_aes(struct Aes128 *aes, uint8_t *memory)
{
    struct Key key;
    struct InitializationVector iv;
    aes128_init2(aes, (uintptr_t)memory, &key, &iv);
}

void test_init()
{
    uint8_t memory[AES128_OUTPUT_STATE+1];
    struct Aes128 aes;
    struct Key key;
    for (size_t i = 0; i < AES128_BLOCK_SIZE; ++i)
        key.key[i] = 255 - (3 * (uint8_t)i);
    struct InitializationVector iv;
    for (size_t i = 0; i < AES128_BLOCK_SIZE; ++i)
        iv.iv[i] = (uint8_t)i;
    aes128_init2(&aes, (uintptr_t)&memory, &key, &iv);
    TEST_ASSERT_EQUAL_PTR(&memory, aes.userlogicOffset);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(key.key, &memory[AES128_INPUT_KEY], AES128_BLOCK_SIZE);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(iv.iv, &memory[AES128_INPUT_IV], AES128_BLOCK_SIZE);
}

void generateData(uint8_t *data, uint8_t count)
{
    for (size_t i = 0; i < count; ++i)
        data[i] = 0xff - (3*(uint8_t)i);
}

void generateResult(uint8_t *result, uint8_t count)
{
    for (size_t i = 0; i < count; ++i) {
        switch (i & 4) {
            case 0:
                result[i] = 0x00 | (0x42-(uint8_t)i);
                break;
            case 1:
                result[i] = 0xef | (0x42-(uint8_t)i);
                break;
            case 2:
                result[i] = 0xcf | (0x42-(uint8_t)i);
                break;
            case 3:
                result[i] = 0xab | (0x42-(uint8_t)i);
                break;
        }
    }
}

void test_encrypt()
{
    uint8_t memory[AES128_OUTPUT_STATE+1];
    uint8_t *inputData = &memory[AES128_INPUT_DATA];
    uint8_t *outputData = &memory[AES128_OUTPUT_DATA];
    uint8_t *state = &memory[AES128_OUTPUT_STATE];

    struct Aes128 aes;
    init_aes(&aes, memory);

    uint8_t data[TEST_DATA_SIZE];
    generateData(data, TEST_DATA_SIZE);
    uint8_t result[TEST_DATA_SIZE];
    generateResult(outputData, TEST_DATA_SIZE);
    *state = AES128_STATE_BIT_READY;
    aes128_encrypt(&aes, data, result, TEST_DATA_SIZE);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(&data[TEST_DATA_SIZE-AES128_BLOCK_SIZE], inputData,
            AES128_BLOCK_SIZE);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(outputData, &result[TEST_DATA_SIZE-AES128_BLOCK_SIZE],
            AES128_BLOCK_SIZE);
}

void test_block_init()
{    
    uint8_t memory[AES128_OUTPUT_STATE+1];
    struct Aes128 aes;
    aes.userlogicOffset = (uintptr_t)&memory;
    struct Block block;

    aes128_block_init(&block, &aes);
    for (size_t i = 0; i < AES128_BLOCK_SIZE; ++i)
        TEST_ASSERT_EQUAL_UINT8(0, block.data[i]);
    TEST_ASSERT_EQUAL_INT(0, block.count);
    TEST_ASSERT_EQUAL_PTR(&aes, block.aes128);
}

void test_block_encrypt()
{
    uint8_t memory[AES128_OUTPUT_STATE+1];
    uint8_t *inputData = &memory[AES128_INPUT_DATA];
    uint8_t *outputData = &memory[AES128_OUTPUT_DATA];
    uint8_t *state = &memory[AES128_OUTPUT_STATE];

    struct Aes128 aes;
    aes.userlogicOffset = (uintptr_t)&memory;
    struct Block input;
    aes128_block_init(&input, &aes);
    generateData(input.data, input.count);
    struct Block output;
    aes128_block_init(&output, &aes);

    generateResult(outputData, AES128_BLOCK_SIZE);
    *state = AES128_STATE_BIT_READY; // = 0 will lead to enless loop
    aes128_block_encrypt(&input, &output);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(inputData, input.data, AES128_BLOCK_SIZE);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(outputData, output.data, AES128_BLOCK_SIZE);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_init);
    RUN_TEST(test_encrypt);
    RUN_TEST(test_block_init);
    RUN_TEST(test_block_encrypt);
    return UNITY_END();
}
