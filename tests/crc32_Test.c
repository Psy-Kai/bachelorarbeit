#define UNITY_INCLUDE_SETUP_STUBS
#include <unity.h>
#include <crc32.h>
#include <crc32def.h>

#define TEST_DATA_SIZE 128

void test_init()
{
    uint8_t memory[CRC32_OUTPUT_STATE+1];
    struct Crc32 crc;
    crc32_init2(&crc, (uintptr_t)&memory);
    TEST_ASSERT_EQUAL_PTR(&memory, crc.userlogicOffset);
}

void test_crc32()
{
    uint8_t memory[CRC32_OUTPUT_STATE+1];
    uint32_t *inputData = (uint32_t*)&memory[CRC32_INPUT_DATA];
    uint32_t *outputData = (uint32_t*)&memory[CRC32_OUTPUT_DATA];
    uint8_t *state = &memory[CRC32_OUTPUT_STATE];

    struct Crc32 crc;
    crc32_init2(&crc, (uintptr_t)&memory);

    uint32_t data[TEST_DATA_SIZE];
    for (uint32_t i = 0; i < TEST_DATA_SIZE; ++i)
        data[i] = i;

    struct Crc32Data crcData;
    crcData.data = data;
    crcData.count = 128;

    const uint32_t outputTestData = 0xff1337ff;
    *outputData = outputTestData;
    *state = (uint8_t)CRC32_OUTPUT_STATE; // = 0 will lead to enless loop
    TEST_ASSERT_EQUAL_UINT32(outputTestData, crc32_calc(&crc, &crcData));
    TEST_ASSERT_EQUAL_UINT32(outputTestData, *outputData);
    TEST_ASSERT_EQUAL_UINT32(TEST_DATA_SIZE-1, *inputData);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_init);
    RUN_TEST(test_crc32);
    return UNITY_END();
}
