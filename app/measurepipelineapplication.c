#include <avr/io.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "lib/debug/debug.h"
#include "lib/stubs/aes128/aes128.h"
#include "lib/stubs/crc32/crc32.h"
#include "lib/flash/flash.h"
#include "lib/fpgaFlash/fpgaFlash.h"
#include "lib/spiArbitration/spiArbitration.h"
#include "lib/xmem/xmem.h"
#include "lib/fpga/fpga.h"
#include "lib/multiboot/multiboot.h"
#include "lib/timer/timer.h"
#include "lib/wireless/HelpersForUsageWithoutCommModule.h"
#include "lib/io/gpio.h"
#include "lib/stubs/ann/ann.h"

#define DATA_SIZE 1500

#define measurepin_white_high() (PORTC |= _BV(PC7))
#define measurepin_white_low() {(PORTC &= ~_BV(PC7)); _delay_ms(1);}

#define main_measurepin_high() measurepin_white_high()
#define main_measurepin_low() measurepin_white_low()

void setup()
{
    debugInit(NULL);

    debugWriteLine("setup phase 1");
    wdt_disable();
    initFlash();
    fpgaFlashInit();

    debugWriteLine("setup phase 2");
    unlockFlash(1);
    deselectFlash(1);

    debugWriteLine("setup phase 3");
    initXmem();
//    enableXmem();
    fpgaInit();
//    fpgaPower(0);
    initMultiboot();

    debugWriteLine("setup phase 4");
    fpgaDisableInterface();

    debugWriteLine("setup phase 5");
    initTimer();

    debugWriteLine("setup phase 6");
    deselectFlash(0);
    unlockFlash(0);
    debugWaitUntilDone();

    debugWriteLine("setup final phase");
    initXmem();

    DDRC = _BV(PC3) | _BV(PC7);
    sei();
}

void initMeasurements()
{
    debugWriteLine("initMeasurements");
    fpgaSetDoneReponse(FPGA_DONE_NOTHING);
    fpgaMultibootClearComplete();
    disableXmem();
    fpgaEnableFlashInterface();
    fpgaPower(1);
    debugWriteLine("ahh shit, here we go again");
    while(!fpgaMultibootComplete());
    debugWriteLine("oh, jeeez");

    enableXmem();
    fpgaSoftReset();

    debugWriteLine("initMeasurements finished");
}

int ann_init()
{
    enableXmem();
    fpgaSoftReset();
    fpgaMultiboot(0x90000);
    fpgaSetDoneReponse(FPGA_DONE_NOTHING);
    while (!fpgaMultibootComplete()) {}
    enableXmem();
    fpgaSoftReset();

    uint8_t id = fpgaGetHWFID();
    debugWriteHex8(id);
    stubAnnWaitBusy();
    if (id != 0xaa)
        return 1;
    return 1;
}

int main(void) {
    setup();
    initMeasurements();

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
    for (;;) {
        /* just measure high values. Low values between measurements should be 1ms long */
        debugWriteLine("loop start");
        /* signal at start of loop (ms steps): __________-----__________-----__________----- */
        measurepin_white_low();
        _delay_ms(9);
        measurepin_white_high();
        _delay_ms(5);
        measurepin_white_low();
        _delay_ms(9);
        measurepin_white_high();
        _delay_ms(5);
        measurepin_white_low();
        _delay_ms(9);
        measurepin_white_high();
        _delay_ms(5);
        measurepin_white_low();

        /*debugWriteLine("ann init");
        main_measurepin_high();
        ann_init();
        main_measurepin_low();

        debugWriteLine("ann learn data");
        main_measurepin_high();
        stubAnnLearn(42, 128);
        main_measurepin_low();

        debugWriteLine("ann query data");
        main_measurepin_high();
        stubAnnQuery(42);
        main_measurepin_low();*/

        uint8_t data[DATA_SIZE];
        memset(data, 0, DATA_SIZE);

        debugWriteLine("aes128_create");
        main_measurepin_high();
        struct Aes128 aes;
        {
            struct Key key;
            for (uint8_t v = 0; v < sizeof(key.key); ++v)
                key.key[v] = v;
            struct InitializationVector iv;
            for (uint8_t v = sizeof(iv.iv); 0 < v; --v)
                iv.iv[v] = v;
            main_measurepin_low();

            debugWriteLine("aes128_init");
            main_measurepin_high();
            aes128_init(&aes, &key, &iv);
            main_measurepin_low();
        }

        debugWriteLine("aes128_encrypt");
        main_measurepin_high();
        aes128_encrypt(&aes, data, data, DATA_SIZE);
        main_measurepin_low();

        debugWriteLine("crc32_create");
        main_measurepin_high();
        struct Crc32 crc32;
        struct Crc32Data crc32Data;
        crc32Data.count = DATA_SIZE;
        crc32Data.data = data;
        main_measurepin_low();

        debugWriteLine("crc32_init");
        main_measurepin_high();
        crc32_init(&crc32);
        main_measurepin_low();

        debugWriteLine("crc32_calc");
        main_measurepin_high();
        crc32_calc(&crc32, &crc32Data);
        main_measurepin_low();
        fpgaPower(0);
    }
#pragma clang diagnostic pop
}
