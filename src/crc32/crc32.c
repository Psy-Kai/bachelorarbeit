#include "crc32.h"

#include <assert.h>
#include <avr/io.h>
#include <string.h>
#if defined(__AVR__)
#include "lib/fpga/fpga.h"
#include "lib/multiboot/multiboot.h"
#include "lib/xmem/xmem.h"
#endif
#include "crc32def.h"

#define measurepin_white_high() (PORTC |= _BV(PC7))
#define measurepin_white_low() {(PORTC &= ~_BV(PC7)); _delay_ms(1);}

#define crc32_measurepin_high() //measurepin_white_high()
#define crc32_measurepin_low() //measurepin_white_low()

inline volatile uint8_t *memOffset(struct Crc32 *thiz, int offset)
{
    return (uint8_t*)thiz->userlogicOffset + offset;
}

inline volatile uint8_t state(struct Crc32 *thiz)
{
    return *memOffset(thiz, CRC32_OUTPUT_STATE);
}

int crc32_configureFpga(struct Crc32 *thiz)
{
    (void)thiz;
#if defined(__AVR__)
    enableXmem();
    fpgaSoftReset();
    fpgaMultiboot(CRC32_FPGA_ADDRESS);
    fpgaSetDoneReponse(FPGA_DONE_NOTHING);
    while (!fpgaMultibootComplete()) {}
    enableXmem();
    fpgaSoftReset();
    uint8_t id = fpgaGetHWFID();
    if (id != CRC32_FPGA_HWFID)
        return 1;
#endif
    return 1;
}

int crc32_init(struct Crc32 *thiz)
{
    if (!crc32_configureFpga(thiz))
        return 0;
#if !defined(__AVR__)
    assert(0);
#else
    crc32_init2(thiz, USERLOGIC_OFFSET);
#endif
    return 1;
}

void crc32_init2(struct Crc32 *thiz, uintptr_t offset)
{
    assert(thiz != NULL);

    thiz->userlogicOffset = offset;
}

uint32_t crc32_calc(struct Crc32 *thiz, struct Crc32Data *crc32Data)
{
    assert(thiz != NULL);
    assert(crc32Data != NULL);
    assert(0 < crc32Data->count);

    volatile uint8_t *input = (uint32_t*)memOffset(thiz, CRC32_INPUT_DATA);
    for (size_t i = 0; i < crc32Data->count; ++i) {
        /* pass data parameter to hardware function */
        crc32_measurepin_high();
        *input = crc32Data->data[i];
        crc32_measurepin_low();

        /* trigger processing of hardware function */
        crc32_measurepin_high();
        *memOffset(thiz, CRC32_INPUT_TRIGGER) = 1;
        crc32_measurepin_low();

        /* busy waiting until hardware-function is ready */
        crc32_measurepin_high();
        while (!(state(thiz) & CRC32_STATE_BIT_READY)) {}
        crc32_measurepin_low();
    }

    /* read result of hardware function */
    crc32_measurepin_high();
    volatile uint32_t result = *(uint32_t*)memOffset(thiz, CRC32_OUTPUT_DATA);
    crc32_measurepin_low();
    return result;
}
