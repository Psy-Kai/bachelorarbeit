library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32Skeleton is
    generic (
        clk_divider : integer := 5000000
    );
    port (
        -- control interface
        clock                : in std_logic;
        reset                : in std_logic; 
        busy                : out std_logic;
                
        -- indicate new data or request
        rd                    : in std_logic;
        wr                     : in std_logic;
        
        -- data interface
        data_in                : in unsigned(7 downto 0);
        address_in            : in unsigned(15 downto 0);
        data_out            : out unsigned(7 downto 0)
    );
end crc32Skeleton;

architecture behavioral of crc32Skeleton is
    component crc32 is
        port (
            clk : in std_logic;
            reset : in std_logic;
            input : in unsigned(7 downto 0);
            output : out unsigned(31 downto 0)
        );
    end component;
    
    signal crc32Clock : std_logic := '0';
    signal crc32Input : unsigned(7 downto 0);
    signal crc32Output : unsigned(31 downto 0);
    signal busyLoopback : std_logic := '0';
begin
    comp : crc32 port map (
        clk => crc32Clock,
        reset => reset,
        input => crc32Input,
        output => crc32Output
    );
    
    busy <= busyLoopback;

    process (clock)
        variable busyVar : std_logic;
    begin
        crc32Clock <= '0';
        if rising_edge(clock)
        then
            busyVar := busyLoopback;
            if reset = '1'
            then
                crc32Input <= to_unsigned(0, crc32Input'length);
                crc32Clock <= '1';
            elsif wr = '0'
            then
                case address_in is
                    when x"0000" => crc32Input <= data_in;
                    when x"00ff" => crc32Clock <= '1';
                    when others =>
                end case;
            elsif rd = '0'
            then
                case address_in is
                    when x"0100" => data_out <= crc32Output(7 downto 0);
                    when x"0101" => data_out <= crc32Output(15 downto 8);
                    when x"0102" => data_out <= crc32Output(23 downto 16);
                    when x"0103" => data_out <= crc32Output(31 downto 24);
                    when x"01ff" =>
                        data_out <= to_unsigned(0, data_out'length);
                        data_out(0) <= not busyVar;
                    when others => 
                end case;
            end if;
            busyLoopback <= busyVar;
        end if;
    end process;

end behavioral;
