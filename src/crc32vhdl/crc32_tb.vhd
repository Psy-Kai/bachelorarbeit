library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32_tb is
end crc32_tb;

architecture tb of crc32_tb is
    component crc32 is
        port (
            clk : in std_logic;
            reset : in std_logic;
            input : in unsigned(7 downto 0);
            output : out unsigned(31 downto 0)
        );
    end component;
    
    signal clk : std_logic := '0';
    signal reset : std_logic := '0';
    signal input : unsigned(7 downto 0);
    signal output : unsigned(31 downto 0);
    signal counter : unsigned(7 downto 0) := to_unsigned(0, 8);
    
    procedure initClock(signal clk : inout std_logic) is
    begin
        clk <= '0';
        wait for 1ns;
    end;
    
    procedure cycle(signal clk : inout std_logic) is
    begin
        clk <= '1';
        wait for 1ns;
        clk <= '0';
        wait for 1ns;
    end;
begin
    comp : crc32 port map (
        clk => clk,
        reset => reset,
        input => input,
        output => output
    );
    
    input <= x"42";
    
    process
    begin
        initClock(clk);
        cycle(clk);
        counter <= counter+1;
    end process;
end tb;
