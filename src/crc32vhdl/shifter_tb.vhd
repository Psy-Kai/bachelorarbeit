library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Shifter_tb is
end Shifter_tb;

architecture tb of Shifter_tb is
    component Shifter is
        port (
            signal input : in unsigned(31 downto 0);
            signal output : out unsigned(31 downto 0)
        );
    end component;
    
    signal input : unsigned(31 downto 0) := to_unsigned(0, 32);
    signal output : unsigned(31 downto 0);
begin
    comp : Shifter port map(
        input => input,
        output => output
    );

    process
    begin
        for i in 0 to 255
        loop
            wait for 1ns;
            input <= to_unsigned(i, input'length);
        end loop;
    end process;
end tb;
