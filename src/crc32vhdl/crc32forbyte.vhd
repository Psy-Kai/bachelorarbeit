library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32ForByte is
    port(
        input : in unsigned(7 downto 0);
        output : out unsigned(31 downto 0)
    );
end crc32ForByte;

architecture rtl of crc32ForByte is
    component Shifter is
        port (
            signal input : in unsigned(31 downto 0);
            signal output : out unsigned(31 downto 0)
        );
    end component;
    
    signal shifterVector : unsigned((32*(8+1))-1 downto 0);
begin
    shifters : for i in 0 to 7 generate
        comp : Shifter port map (
            input => shifterVector((32*i)+31 downto 32*i),
            output => shifterVector((32*(i+1))+31 downto 32*(i+1))
        );
    end generate;

    shifterVector(31 downto 8) <= to_unsigned(0, 24);
    shifterVector(7 downto 0) <= input;
    output <= shifterVector((32*9)-1 downto (32*8)) xor x"ff000000";
end rtl;
