library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32 is
    port (
        clk : in std_logic;
        reset : in std_logic;
        input : in unsigned(7 downto 0);
        output : out unsigned(31 downto 0)
    );
end crc32;

architecture rtl of crc32 is
    component crc32ForByte is
    port(
        input : in unsigned(7 downto 0);
        output : out unsigned(31 downto 0)
    );
    end component;
    
    signal byteInput : unsigned(7 downto 0);
    signal byteOutput : unsigned(31 downto 0);
    signal outputLoopback : unsigned(31 downto 0) := to_unsigned(0, 32);
begin
    comp : crc32ForByte port map (
        input => byteInput,
        output => byteOutput
    );
    
    byteInput <= input xor outputLoopback(7 downto 0);
    output <= outputLoopback;
    
    process(clk)
    begin
        if rising_edge(clk)
        then    
            if reset = '1'
            then
                outputLoopback <= to_unsigned(0, outputLoopback'length);
            else
                outputLoopback <= byteOutput xor shift_right(outputLoopback, 8);
            end if;        
        end if;
    end process;
end rtl;
