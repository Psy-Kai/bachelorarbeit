library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity crc32MeasuringTest_tb is
end crc32MeasuringTest_tb;

architecture tb of crc32MeasuringTest_tb is
    component crc32MeasuringTest is
    port(
        clk : in std_logic;        
        triggerMeasurePin : out std_logic;
        outputReadyMeasurePin : out std_logic
    );
    end component;
    
    signal clk : std_logic := '0';
    signal triggerMeasurePin : std_logic;
    signal outputReadyMeasurePin : std_logic;
    
    procedure clock(signal clk : inout std_logic) is
    begin
        clk <= '1';
        wait for 1ns;
        clk <= '0';
        wait for 1ns;
    end;
begin
    comp : crc32MeasuringTest port map (
        clk => clk,
        triggerMeasurePin => triggerMeasurePin,
        outputReadyMeasurePin => outputReadyMeasurePin
    );

    process
    begin
        clock(clk);
    end process;
end tb;
