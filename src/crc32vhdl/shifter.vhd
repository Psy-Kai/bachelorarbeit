library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Shifter is
    port (
        signal input : in unsigned(31 downto 0);
        signal output : out unsigned(31 downto 0)
    );
end Shifter;

architecture behavioral of Shifter is
begin
    with input(0) select output <=
         shift_right(input, 1) when '1',
         shift_right(input, 1) xor x"edb88320" when others;
end behavioral;
