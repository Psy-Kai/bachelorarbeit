library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32ForByte_tb is
end crc32ForByte_tb;

architecture tb of crc32ForByte_tb is
    component crc32ForByte is
        port(
            input : in unsigned(7 downto 0);
            output : out unsigned(31 downto 0)
        );
    end component;

    signal input : unsigned(7 downto 0) := to_unsigned(0, 8);
    signal output : unsigned(31 downto 0);
begin
    comp : crc32ForByte port map (
        input => input,
        output => output
    );

    process
    begin
        wait for 1ns;
        input <= input + 1;
    end process;
end tb;
