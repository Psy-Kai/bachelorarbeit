library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32Skeleton_tb is
end crc32skeleton_tb;

architecture tb of crc32Skeleton_tb is
    component crc32Skeleton is
        generic (
            clk_divider : integer := 5000000
        );
        port (
            -- control interface
            clock                : in std_logic;
            reset                : in std_logic; 
            busy                : out std_logic;
                    
            -- indicate new data or request
            rd                    : in std_logic;
            wr                     : in std_logic;
            
            -- data interface
            data_in                : in unsigned(7 downto 0);
            address_in            : in unsigned(15 downto 0);
            data_out            : out unsigned(7 downto 0)
        );
    end component;
    
    signal clock : std_logic := '0';
    signal reset : std_logic := '0';
    signal busy : std_logic := '0';
    
    signal rd : std_logic := '1';
    signal wr : std_logic := '1';
    
    signal data_in : unsigned(7 downto 0) := to_unsigned(0, 8);
    signal address_in : unsigned(15 downto 0) := to_unsigned(0, 16);
    signal data_out : unsigned(7 downto 0) := to_unsigned(0, 8);
    
    signal output : unsigned(31 downto 0) := to_unsigned(0, 32);
    
    procedure clockInit(signal clock : out std_logic) is
    begin
        clock <= '0';
        wait for 1ns;
    end;
    procedure clockCycle(signal clock : out std_logic) is
    begin
        clock <= '1';
        wait for 1ns;
        clock <= '0';
        wait for 1ns;
    end;  
    
    procedure triggerReset(signal reset : out std_logic; signal clock : out std_logic) is
    begin
        reset <= '1';
        clockCycle(clock);
        reset <= '0';
        clockCycle(clock);
    end;
    
    procedure read(value : out unsigned(7 downto 0); address : unsigned(15 downto 0);
    signal dataSignal : in unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal readSignal : out std_logic; signal clock : out std_logic) is
    begin
        addressSignal <= address;
        readSignal <= '0';
        clockCycle(clock);
        value := dataSignal;
        readSignal <= '1';
    end;
    
    procedure readBlock(value : out unsigned(31 downto 0); startingAddress : unsigned(15 downto 0);
    signal dataSignal : in unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal readSignal : out std_logic; signal clock : out std_logic) is
    begin
        for i in 0 to 3 loop
            read(value((8*i)+7 downto 8*i), startingAddress+i, dataSignal, addressSignal, readSignal, clock);
        end loop;
    end;
    
    procedure write(value : unsigned(7 downto 0); address : unsigned(15 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        dataSignal <= value;
        addressSignal <= address;
        writeSignal <= '0';
        clockCycle(clock);
        writeSignal <= '1';
    end;
    
    procedure writeBlock(value : unsigned(31 downto 0); startingAddress : unsigned(15 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        for i in 0 to 3 loop
            write(value((8*i)+7 downto 8*i), startingAddress+i, dataSignal, addressSignal, writeSignal, clock);
        end loop;
    end;
begin
    comp : crc32Skeleton port map (
        clock => clock,
        reset => reset,
        busy => busy,
        rd => rd,
        wr => wr,
        data_in => data_in,
        address_in => address_in,
        data_out => data_out
    );
    
    process
        variable outputVar : unsigned(31 downto 0);
    begin
        clockInit(clock);
        triggerReset(reset, clock);
        
        for i in 0 to 65535
        loop
            write(to_unsigned(i, 8), x"0000", data_in, address_in, wr, clock);
            write(to_unsigned(1, 8), x"00ff", data_in, address_in, wr, clock);
            clockCycle(clock);
            readBlock(outputVar, x"0100", data_out, address_in, rd, clock);
            output <= outputVar;
            clockCycle(clock);
        end loop;
    end process;
end tb;
