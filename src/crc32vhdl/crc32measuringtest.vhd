library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crc32MeasuringTest is
    port(
        clk : in std_logic;        
        triggerMeasurePin : out std_logic;
        outputReadyMeasurePin : out std_logic
    );
end crc32MeasuringTest;

architecture behavioral of crc32MeasuringTest is
    component crc32 is
        port (
            clk : in std_logic;
            reset : in std_logic;
            input : in unsigned(7 downto 0);
            output : out unsigned(31 downto 0)
        );
    end component;
    
    signal reset : std_logic := '0';
    signal input : unsigned(7 downto 0);
    signal output : unsigned(31 downto 0);
begin
    comp : crc32 port map (
        clk => clk,
        reset => reset,
        input => input,
        output => output
    );

    input <= x"42";
    
    process (clk)
    begin
        if rising_edge(clk)
        then
            if reset = '0'
            then
                reset <= '1';
                triggerMeasurePin <= '1';
                outputReadyMeasurePin <= '0';
            else
                reset <= '0';
                triggerMeasurePin <= '0';
                outputReadyMeasurePin <= '1';                
            end if;
        end if;
    end process;
end behavioral;
