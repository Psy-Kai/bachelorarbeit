library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity AES128_cbc_tb is
end AES128_cbc_tb;

architecture tb of AES128_cbc_tb is
    component AES128_cbc is
         port (
            trigger : in std_logic;
            clk : in std_logic;
            initializationVector : in std_logic_vector(aes.kAesSize-1 downto 0);
            input : in std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            output : out std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
            outputReady : out std_logic := '0'
        );
    end component;

    signal trigger : std_logic := '0';
    signal clk : std_logic := '0';
    signal iV : std_logic_vector(aes.kAesSize-1 downto 0);
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
    signal outputReady : std_logic;
begin
    comp : AES128_cbc port map (
        trigger => trigger,
        clk => clk,
        initializationVector => iV,
        input => input,
        key => key,
        output => output,
        outputReady => outputReady
    );
    
    input <= x"3243f6a8885a308d313198a2e0370734";
    key <= x"2b7e151628aed2a6abf7158809cf4f3c";
    iV <= x"00000000000000000000000000000000";
    
    process
    begin
        clk <= '0';
        
        for i in 0 to 100
        loop
            clk <= not clk;
            wait for 150ps;
        end loop;
    
        trigger <= '1';  
        clk <= not clk;
        wait for 1ns;
        clk <= not clk;
        wait for 1ns;
        trigger <= '0';
        clk <= not clk;
        wait for 1ns;
        clk <= not clk;
        wait for 1ns;
        
        while (outputReady = '0')
        loop
            clk <= not clk;
            wait for 1ns;
        end loop;        
        
        for i in 0 to 100
        loop
            clk <= not clk;
            wait for 150ps;
        end loop;
    end process;
end tb;
