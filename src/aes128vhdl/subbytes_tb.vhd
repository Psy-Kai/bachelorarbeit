library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity SubBytes_tb is
end SubBytes_tb;

architecture tb of SubBytes_tb is
    component SubBytes is
    port(
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
    end component;
    
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
begin
    comp: SubBytes port map (
        input => input,
        output => output
    );
    
    process
    begin
        for x in 0 to (aes.kColumnCount*aes.kRowCount)-1 loop
            input(aes.kAesSize-1 downto 0) <= std_logic_vector(to_unsigned(0, input'length));
            for i in 0 to 255 loop
                input((x*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto x*aes.kBitsPerByte) <= std_logic_vector(to_unsigned(i, aes.kBitsPerByte));
                wait for 10 ns;
            end loop; 
        end loop;
    end process;
end tb;
