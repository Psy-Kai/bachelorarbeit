library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.UNIFORM;

entity ApplyKey_tb is
end ApplyKey_tb;

architecture tb of ApplyKey_tb is
    component ApplyKey is
        port(
            input : in std_logic_vector(127 downto 0);
            output : out std_logic_vector(127 downto 0);
            key : in std_logic_vector(127 downto 0)
        );
    end component;

    signal input : std_logic_vector(127 downto 0);
    signal key : std_logic_vector(127 downto 0);
    signal output : std_logic_vector(127 downto 0);
begin
    comp: ApplyKey port map (
        input => input,
        key => key,
        output => output
    );

    process
    begin            
        for pos in 0 to (input'length/8)-1
        loop
            input <= std_logic_vector(to_unsigned(0, input'length));
            key <= std_logic_vector(to_unsigned(0, key'length));
            for inputValue in 0 to 255
            loop
                input((pos*8)+7 downto pos*8) <= std_logic_vector(to_unsigned(inputValue, 8));     
                for keyValue in 0 to 255
                loop
                    key((pos*8)+7 downto pos*8) <= std_logic_vector(to_unsigned(keyValue, 8));
                wait for 10 us;                    
                end loop;                    
            end loop;
        end loop;
    end process;
end tb;
