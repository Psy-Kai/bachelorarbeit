library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.AES;

entity GaloisMultiplication_Mult2 is
    port (
        input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
        output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
    );
end GaloisMultiplication_Mult2;

architecture behavioral of GaloisMultiplication_Mult2 is
    signal mult2 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
begin
    mult2(aes.kBitsPerByte-1 downto 1) <= input(aes.kBitsPerByte-2 downto 0);
    mult2(0) <= '0';
    
    output <= mult2 when input < x"80" else
              mult2 xor x"1b";
end behavioral;
