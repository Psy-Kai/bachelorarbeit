library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.aes;

entity ShiftRows is
    port(
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        
        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
end ShiftRows;

architecture behavioral of ShiftRows is
begin
    newColumn0((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte) <=
        column3((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte);
    newColumn1((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte) <=
        column0((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte);
    newColumn2((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte) <=
        column1((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte);
    newColumn3((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte) <=
        column2((0*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 0*aes.kBitsPerByte);
        
    newColumn0((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte) <=
        column2((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte);
    newColumn1((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte) <=
        column3((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte);
    newColumn2((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte) <=
        column0((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte);
    newColumn3((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte) <=
        column1((1*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 1*aes.kBitsPerByte);
        
    newColumn0((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte) <=
        column1((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte);
    newColumn1((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte) <=
        column2((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte);
    newColumn2((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte) <=
        column3((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte);
    newColumn3((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte) <=
        column0((2*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 2*aes.kBitsPerByte);
        
    newColumn0((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte) <=
        column0((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte);
    newColumn1((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte) <=
        column1((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte);
    newColumn2((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte) <=
        column2((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte);
    newColumn3((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte) <=
        column3((3*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto 3*aes.kBitsPerByte);
end behavioral;
