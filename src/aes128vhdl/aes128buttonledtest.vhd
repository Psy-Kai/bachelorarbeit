library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity AES128ButtonLedTest is
    generic (
        debounceClkCount : unsigned := to_unsigned(127, 7)
    );
    port (
        clk : in std_logic;
        button : in std_logic;                      -- BTN3
        ledSuccess : out std_logic := '0';          -- LD1 green
        ledError : out std_logic := '0';            -- LD1 red
        ledOutputReady : out std_logic := '0';      -- LD0 green
        ledTrigger : out std_logic := '0';          -- LD0 blue
        ledTestOutput : out std_logic_vector(3 downto 0)     -- LD5  dwonto LD2
    );
end AES128ButtonLedTest;

architecture behavioral of AES128ButtonLedTest is 
    component AES128_cbc is
        port (
            trigger : in std_logic;
            clk : in std_logic;
            initializationVector : in std_logic_vector(aes.kAesSize-1 downto 0);
            input : in std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            output : out std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
            outputReady : out std_logic := '0'
        );
    end component;
            
    component Debounce is
        generic (
            clkCount : unsigned
        );
        port (
            input : in std_logic;
            clk : in std_logic;
            reset : in std_logic;
            output : out std_logic
        );
    end component;
    
    signal inputCounter : unsigned(3 downto 0) := "0000";
    
    signal trigger : std_logic := '0';
    signal iV : std_logic_vector(aes.kAesSize-1 downto 0);
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
    signal outputReady : std_logic;
    
    signal ledTriggerLoopback : std_logic := '0';
    signal ledTestOutputLoopback : std_logic_vector(ledTestOutput'length-1 downto 0) := 
        std_logic_vector(to_unsigned(0, ledTestOutput'length));
begin
    comp : AES128_cbc port map (
        trigger => trigger,
        clk => clk,
        initializationVector => iV,
        input => input,
        key => key,
        output => output,
        outputReady => outputReady
    );
    debounceComp : Debounce generic map (
        clkCount => debounceClkCount
    )
    port map (
        input => button,
        clk => clk,
        reset => '0',
        output => trigger
    ); 
    
    iV <= std_logic_vector(to_unsigned(0, aes.kAesSize));
    input <= x"3243f6a8885a308d313198a2e0370734";
    key <= x"2b7e151628aed2a6abf7158809cf4f3c";
    ledTrigger <= ledTriggerLoopback;
    ledOutputReady <= outputReady;
    ledTestOutput <= ledTestOutputLoopback;

    process (trigger)
        variable ledTriggerLoopbackVar : std_logic;
        variable counter : unsigned(inputCounter'length-1 downto 0);
    begin
        if (rising_edge(trigger))
        then
            counter := inputCounter;
            ledTriggerLoopbackVar := ledTriggerLoopback;
            
            counter := counter+1; 
            ledTriggerLoopbackVar := not ledTriggerLoopbackVar;
            
            ledTriggerLoopback <= ledTriggerLoopbackVar;
            inputCounter <= counter;
        end if;
    end process;
    
    process (clk)
        variable succ : std_logic;
    begin
        if (rising_edge(clk))
        then
            succ := '0';
            if (outputReady = '1')
            then
                case inputCounter is
                    when "0001" => if (output = x"3925841d02dc09fbdc118597196a0b32") then succ := '1'; end if;
                    when "0010" => if (output = x"ebbb47f679290b492c9154ce9cd97f83") then succ := '1'; end if;
                    when "0011" => if (output = x"b3a31631c0a24f8c533d964c861d1747") then succ := '1'; end if;
                    when "0100" => if (output = x"ead15bc017132fe8a9d32067c0091159") then succ := '1'; end if;
                    when "0101" => if (output = x"5008513d35a6b84239d7595503757eaa") then succ := '1'; end if;
                    when "0110" => if (output = x"1132e9c60bc038e616777363f2514321") then succ := '1'; end if;
                    when "0111" => if (output = x"60d24d59ed77376286c7a31099dbf1aa") then succ := '1'; end if;
                    when "1000" => if (output = x"71496059bae8034dcd0f8d6a57186f5f") then succ := '1'; end if;
                    when "1001" => if (output = x"c116f0002b78551910afce293242f6d5") then succ := '1'; end if;
                    when "1010" => if (output = x"4a1b5ac3b8c02c38abf9c205a6379ce6") then succ := '1'; end if;
                    when others => succ := '0';
                end case;
    
                ledTestOutputLoopback <= output(3 downto 0);
                
                if (succ = '1')
                then
                    ledSuccess <= '1';
                    ledError <= '0';
                else
                    ledSuccess <= '0';
                    ledError <= '1';
                end if;
            end if;
        end if;
    end process;
end behavioral;
