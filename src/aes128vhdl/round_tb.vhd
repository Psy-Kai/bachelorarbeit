library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity Round_tb is
end Round_tb;

architecture tb of Round_tb is
    component Round is
        port (
            round : in std_logic_vector(3 downto 0);
            state : in std_logic_vector(aes.kAesSize-1 downto 0);
            newState : out std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
        );
    end component;  
    
    signal rount : std_logic_vector(3 downto 0);
    signal state : std_logic_vector(aes.kAesSize-1 downto 0);
    signal newState : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal newKey : std_logic_vector(aes.kAesSize-1 downto 0);
begin
    comp : Round port map (
        round => rount,
        state => state,
        newState => newState,
        key => key,
        newKey => newKey
    );
    
    process
    begin
        state <= x"193de3bea0f4e22b9ac68d2ae9f84808";
        key <= x"a0fafe1788542cb123a339392a6c7605";
        for r in 2 to 10
        loop
            rount <= std_logic_vector(to_unsigned(r, rount'length));
            wait for 10 ns; 
            state <= newState;
            key <= newKey;
        end loop;
    end process;
end tb;
