library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity AES128MeasuringTest is
    generic (
        debounceClkCount : unsigned := to_unsigned(127, 7)
    );
    port (
        clk : in std_logic;
        triggerMeasurePin : out std_logic;
        outputReadyMeasurePin : out std_logic
    );
end AES128MeasuringTest;

architecture behavioral of AES128MeasuringTest is 
    component AES128_cbc is
        port (
            trigger : in std_logic;
            clk : in std_logic;
            initializationVector : in std_logic_vector(aes.kAesSize-1 downto 0);
            input : in std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            output : out std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
            outputReady : out std_logic := '0'
        );
    end component;
    
    signal trigger : std_logic := '0';
    signal triggered : std_logic := '0';
    signal triggerDummy : std_logic := '0';
    signal iV : std_logic_vector(aes.kAesSize-1 downto 0);
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
    signal outputReady : std_logic;
begin
    comp : AES128_cbc port map (
        trigger => trigger,
        clk => clk,
        initializationVector => iV,
        input => input,
        key => key,
        output => output,
        outputReady => outputReady
    ); 
    
    iV <= std_logic_vector(to_unsigned(0, aes.kAesSize));
    input <= x"3243f6a8885a308d313198a2e0370734";
    key <= x"2b7e151628aed2a6abf7158809cf4f3c";
    triggerMeasurePin <= trigger;
    outputReadyMeasurePin <= outputReady;
    
    process (clk)
        variable succ : std_logic;
        variable triggeredVar : std_logic;
    begin
        if (rising_edge(clk))
        then
            triggeredVar := triggered;
            if (outputReady = '1')
            then
                trigger <= '1';
                triggeredVar := '1';
            else
                if (triggeredVar = '0')
                then
                    trigger <= '1';
                    triggeredVar := '1';
                else
                    trigger <= '0';
                end if;
            end if;
            triggered <= triggeredVar;
        end if;
    end process;
end behavioral;
