package aes is
constant kColumnCount, kRowCount : integer := 4;
constant kBitsPerByte : integer := 8;
constant kBitsPerColumn : integer := kColumnCount*kBitsPerByte;
constant kBitsPerRow : integer := kRowCount*kBitsPerByte;
constant kAesSize : integer := kColumnCount * kRowCount * kBitsPerByte;
end package;