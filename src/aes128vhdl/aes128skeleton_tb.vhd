library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity Aes128Skeleton_tb is
end Aes128Skeleton_tb;

architecture tb of Aes128Skeleton_tb is
    component Aes128Skeleton is
        generic (
            clk_divider : integer := 5000000
            );
        port (
            -- control interface
            clock				: in std_logic;
            reset				: in std_logic; -- controls functionality (sleep)
            busy				: out std_logic; -- done with entire calculation
                    
            -- indicate new data or request
            rd		            : in std_logic;	-- request a variable
            wr 				    : in std_logic; 	-- request changing a variable
            
            -- data interface
            data_in			    : in unsigned(7 downto 0);
            address_in		    : in unsigned(15 downto 0);
            data_out			: out unsigned(7 downto 0)
        );
    end component;
    
    signal clock : std_logic := '0';
    signal reset : std_logic := '0';
    signal busy : std_logic := '0';
    
    signal rd : std_logic := '1';
    signal wr : std_logic := '1';
    
    signal data_in : unsigned(7 downto 0) := to_unsigned(0, 8);
    signal address_in : unsigned(15 downto 0) := to_unsigned(0, 16);
    signal data_out : unsigned(7 downto 0) := to_unsigned(0, 8);
    
    signal iV : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
    signal stateOutput : unsigned(7 downto 0);
    
    procedure clockInit(signal clock : out std_logic) is
    begin
        clock <= '0';
        wait for 1ns;
    end;
    procedure clockCycle(signal clock : out std_logic) is
    begin
        clock <= '1';
        wait for 1ns;
        clock <= '0';
        wait for 1ns;
    end;  
    
    procedure triggerReset(signal reset : out std_logic; signal clock : out std_logic) is
    begin
        reset <= '1';
        clockCycle(clock);
        reset <= '0';
        clockCycle(clock);
    end;
    
    procedure read(value : out unsigned(7 downto 0); address : unsigned(15 downto 0);
    signal dataSignal : in unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal readSignal : out std_logic; signal clock : out std_logic) is
    begin
        addressSignal <= address;
        readSignal <= '0';
        clockCycle(clock);
        value := dataSignal;
        readSignal <= '1';
    end;
    
    procedure readBlock(value : out unsigned(aes.kAesSize-1 downto 0); startingAddress : unsigned(15 downto 0);
    signal dataSignal : in unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal readSignal : out std_logic; signal clock : out std_logic) is
    begin
        for i in 0 to 15 loop
            read(value((8*i)+7 downto 8*i), startingAddress+i, dataSignal, addressSignal, readSignal, clock);
        end loop;
    end;
    
    procedure write(value : unsigned(7 downto 0); address : unsigned(15 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        dataSignal <= value;
        addressSignal <= address;
        writeSignal <= '0';
        clockCycle(clock);
        writeSignal <= '1';
    end;
    
    procedure writeBlock(value : unsigned(aes.kAesSize-1 downto 0); startingAddress : unsigned(15 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        for i in 0 to 15 loop
            write(value((8*i)+7 downto 8*i), startingAddress+i, dataSignal, addressSignal, writeSignal, clock);
        end loop;
    end;
    
    procedure writeInput(value : unsigned(aes.kAesSize-1 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        writeBlock(value, x"0000", dataSignal, addressSignal, writeSignal, clock);
    end;    
    procedure writeKey(value : unsigned(aes.kAesSize-1 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        writeBlock(value, x"0010", dataSignal, addressSignal, writeSignal, clock);
    end;
    procedure writeInitializationVector(value : unsigned(aes.kAesSize-1 downto 0); 
    signal dataSignal : out unsigned(7 downto 0); signal addressSignal : out unsigned(15 downto 0); signal writeSignal : out std_logic; signal clock : out std_logic) is
    begin
        writeBlock(value, x"0020", dataSignal, addressSignal, writeSignal, clock);
    end;
begin
    comp : Aes128Skeleton port map (
        clock => clock,
        reset => reset,
        busy => busy,
        rd => rd,
        wr => wr,
        data_in => data_in,
        address_in => address_in,
        data_out => data_out
    );
    
    iV <= x"00000000000000000000000000000000";
    key <= x"2b7e151628aed2a6abf7158809cf4f3c";

    process
        variable outputVar : unsigned(aes.kAesSize-1 downto 0);
        variable stateOutputVar : unsigned(7 downto 0);
    begin
        clockInit(clock);
        triggerReset(reset, clock);
        clockCycle(clock);
        writeInitializationVector(unsigned(iV), data_in, address_in, wr, clock);
        writeKey(unsigned(key), data_in, address_in, wr, clock);
        input <= x"3243f6a8885a308d313198a2e0370734";
        clockCycle(clock);
        writeInput(unsigned(input), data_in, address_in, wr, clock);
        
        for i in 0 to 10
        loop
            write(x"00", x"00FF", data_in, address_in, wr, clock);
            clockCycle(clock);
            stateOutputVar := to_unsigned(0, stateOutput'length);
            while stateOutputVar /= to_unsigned(1, stateOutput'length) loop
                read(stateOutputVar, x"01ff", data_out, address_in, rd, clock);
                stateOutput <= stateOutputVar;
--                clockCycle(clock);
            end loop;
            readBlock(outputVar, x"0100", data_out, address_in, rd, clock);
            output <= std_logic_vector(outputVar);
            clockCycle(clock);
        end loop;
    end process;
end tb;
