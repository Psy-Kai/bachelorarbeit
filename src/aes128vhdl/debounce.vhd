library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- https://www.digikey.com/eewiki/pages/viewpage.action?pageId=4980758

entity Debounce is
    generic (
        clkCount : unsigned := to_unsigned(42, 5)
    );
    port (
        input : in std_logic;
        clk : in std_logic;
        reset : in std_logic;
        output : out std_logic
    );
end Debounce;

architecture behavioral of Debounce is
    signal state : std_logic;
    signal lastState : std_logic;
    signal counter : unsigned(clkCount'length-1 downto 0);
    signal counterReset : std_logic;
    signal outputLoopback : std_logic := '0';
begin
    counterReset <= reset or (state xor lastState);

    process (clk, input, state)
        variable s : std_logic;
    begin
        s := state;
        if (rising_edge(clk))
        then
            state <= input;
            lastState <= s;
        end if;
    end process;
    
    output <= outputLoopback;
    
    count : process (clk, counterReset, counter, lastState, outputLoopback)
        variable counterVar : unsigned(counter'length-1 downto 0);
        variable outputVar : std_logic;
    begin
        counterVar := counter;
        outputVar := outputLoopback;
        if (rising_edge(clk))
        then
            if (counterReset = '1')
            then
                counterVar := to_unsigned(0, counter'length);
            else
                counterVar := counterVar + 1;
                if (counterVar = clkCount)
                then
                    outputVar := lastState;
                    counterVar := to_unsigned(0, counter'length);                 
                end if;
            end if;
        end if;
        counter <= counterVar;
        outputLoopback <= outputVar;
    end process;
end behavioral;
