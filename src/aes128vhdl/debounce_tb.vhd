library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Debounce_tb is
end Debounce_tb;

architecture tb of Debounce_tb is
    component Debounce is
        generic (
            clkCount : unsigned
        );
        port (
            input : in std_logic;
            clk : in std_logic;
            reset : in std_logic;
            output : out std_logic
        );
    end component;
    
    signal input : std_logic;
    signal clk : std_logic;
    signal reset : std_logic;
    signal output : std_logic;
begin
    comp : Debounce generic map (
        clkCount => to_unsigned(10, 4)
    )
    port map (
        input => input,
        clk => clk,
        reset => reset,
        output => output
    );
    
    process
    begin
        
    end process;
end tb;
