library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity GaloisMultiplication is
    generic (
        kMultiplicand : integer        
    );
    port (
        input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
        output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
    );
end GaloisMultiplication;

architecture behavioral of GaloisMultiplication is
    component GaloisMultiplication_Mult2 is
    port (
        input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
        output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
    );
    end component;
    
    signal mult2 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
begin
    mult2Comp : GaloisMultiplication_Mult2 port map (
        input => input,
        output => mult2
    );
    
    with kMultiplicand select output <=
        input when 1,
        mult2 when 2,
        mult2 xor input when 3,
        x"00" when others;
end behavioral;
