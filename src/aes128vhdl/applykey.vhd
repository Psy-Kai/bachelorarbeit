library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.AES;

entity ApplyKey is
    port(
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0);
        key : in std_logic_vector(aes.kAesSize-1 downto 0)
    );
end ApplyKey;

architecture behavioral of ApplyKey is
begin
    output <= input xor key;
end behavioral;
