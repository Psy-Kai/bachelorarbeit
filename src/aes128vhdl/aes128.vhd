library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;
-- matrix encoding:
--  --               --
--  | b0  b4  b8  b12 |
--  | b1  b5  b9  b13 |
--  | b2  b6  b10 b14 |
--  | b3  b7  b11 b15 | 
--  --               --
-- will be encoded as
--   state(aes.kAesSize-1 downto 0) with [b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b3, b14, b15]
-- this means
--   state(  0*kBitsPerByte + aes.kBitsPerByte-1 downto   0*kBitsPerByte) = b15
--   state(  1*kBitsPerByte + aes.kBitsPerByte-1 downto   1*kBitsPerByte) = b14
--   state(  2*kBitsPerByte + aes.kBitsPerByte-1 downto   2*kBitsPerByte) = b13
--   state(n-2*kBitsPerByte + aes.kBitsPerByte-1 downto n-2*kBitsPerByte) = b1
--   state(n-1*kBitsPerByte + aes.kBitsPerByte-1 downto n.1*kBitsPerByte) = b0


-- columns are inverted:
--  --    --
--  | b0   |
--  | b1   |
--  | b2   |
--  | ...  |
--  | bn-2 |
--  | bn-1 |
--  --    --
-- will be encoded as
--   column(aes.kBitsPerColumn-1 downto 0) with (b0, b1, b2, ..., bn-2, bn-1)
-- this means
--   column(  0*kBitsPerByte + aes.kBitsPerByte-1 downto   0*kBitsPerByte) = bn-1
--   column(  1*kBitsPerByte + aes.kBitsPerByte-1 downto   1*kBitsPerByte) = bn-2
--   column(  2*kBitsPerByte + aes.kBitsPerByte-1 downto   2*kBitsPerByte) = bn-3
--   column(n-2*kBitsPerByte + aes.kBitsPerByte-1 downto n-2*kBitsPerByte) = b1
--   column(n-1*kBitsPerByte + aes.kBitsPerByte-1 downto n-1*kBitsPerByte) = b0

entity AES128 is
    port (
        clk : in std_logic;
        reset : in std_logic;
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0);
        outputReady : out std_logic := '0'
    );
end AES128;

architecture rtl of AES128 is
    component AddRoundKey is
        port(
            round : in unsigned(3 downto 0);
            state : in std_logic_vector(aes.kAesSize-1 downto 0);
            newState : out std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
        );
    end component;
    component Round is
        port (
            round : in unsigned(3 downto 0);
            state : in std_logic_vector(aes.kAesSize-1 downto 0);
            newState : out std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
        );
    end component;
    component SubBytes is
    port(
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
    end component;
    component ShiftRows is
    port(
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        
        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
    end component;
    component MixColumns is
    port (
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        
        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
    end component;
   
    signal addRoundKeyBeginNewState : std_logic_vector(aes.kAesSize-1 downto 0);
    signal addRoundKeyBeginNewKey : std_logic_vector(aes.kAesSize-1 downto 0);  
      
    signal roundCounter : unsigned(3 downto 0) := "0001";
    signal roundState : std_logic_vector(aes.kAesSize-1 downto 0);
    signal roundNewState : std_logic_vector(aes.kAesSize-1 downto 0);
    signal roundKey : std_logic_vector(aes.kAesSize-1 downto 0);
    signal roundNewKey : std_logic_vector(aes.kAesSize-1 downto 0);
    
    signal subbedBytesRoundEnd : std_logic_vector(aes.kAesSize-1 downto 0);
    signal shiftedRowsRoundEnd : std_logic_vector(aes.kAesSize-1 downto 0);
    
    signal outputReadyLoopback : std_logic;
begin
    addRoundKeyBegin : AddRoundKey port map (
        round => x"1", 
        state => input,
        newState => addRoundKeyBeginNewState,
        key => key,
        newKey => addRoundKeyBeginNewKey
    );
    roundComp : Round port map (
        round => roundCounter,
        state => roundState,
        newState => roundNewState,
        key => roundKey,
        newKey => roundNewKey
    );
    subBytesCompEnd : SubBytes port map (
        input => roundNewState,
        output => subbedBytesRoundEnd
    );
    shiftRowsCompEnd : ShiftRows port map (
        column0 => subbedBytesRoundEnd((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        column1 => subbedBytesRoundEnd((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        column2 => subbedBytesRoundEnd((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        column3 => subbedBytesRoundEnd((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn),
        newColumn0 => shiftedRowsRoundEnd((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        newColumn1 => shiftedRowsRoundEnd((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        newColumn2 => shiftedRowsRoundEnd((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        newColumn3 => shiftedRowsRoundEnd((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn) 
    );    
    addRoundKeyEnd : AddRoundKey port map (
        round => x"a",
        state => shiftedRowsRoundEnd,
        newState => output,
        key => roundNewKey
    );
    
    outputReady <= outputReadyLoopback;
    
    process (clk)
        variable counter : unsigned(3 downto 0);
        variable ready : std_logic;
    begin
        if (rising_edge(clk))
        then
            counter := roundCounter;
            ready := '0';
            if (reset = '1') 
            then
                counter := to_unsigned(1, roundCounter'length);
            else        
                counter := counter + 1;
                case counter is
                    when x"2" => -- see addRoundKeyBegin.round
                        roundState <= addRoundKeyBeginNewState;
                        roundKey <= addRoundKeyBeginNewKey;
                    when x"a" =>
                        roundState <= roundNewState;
                        roundKey <=  roundNewKey;
                        ready := '1';
                    when others =>
                        roundState <= roundNewState;
                        roundKey <=  roundNewKey;
                end case;
            end if;
            outputReadyLoopback <= ready;
            roundCounter <= counter;
        end if;
    end process;
end rtl;
