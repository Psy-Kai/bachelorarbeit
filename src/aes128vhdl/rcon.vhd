library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Rcon is
    port (
        input : in std_logic_vector(31 downto 0);
        round : in unsigned(3 downto 0);
        output : out std_logic_vector(31 downto 0)
    );
end Rcon;

architecture behavioral of Rcon is
begin
    with round select output <=
        input xor x"01000000" when x"1",
        input xor x"02000000" when x"2",
        input xor x"04000000" when x"3",
        input xor x"08000000" when x"4",
        input xor x"10000000" when x"5",
        input xor x"20000000" when x"6",
        input xor x"40000000" when x"7",
        input xor x"80000000" when x"8",
        input xor x"1b000000" when x"9",
        input xor x"36000000" when x"a",
        input xor x"00000000" when others;
end behavioral;
