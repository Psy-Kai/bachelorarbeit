library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity XorKeys is
    port (
        key : in std_logic_vector(127 downto 0);
        rconedKeyColumn : in std_logic_vector(31 downto 0);
        newKey : out std_logic_vector(127 downto 0)
    );
end XorKeys;

architecture behavioral of XorKeys is
    signal newKeyBuf : std_logic_vector(127 downto 0);
begin
    newKeyBuf((15*8)+7 downto 12*8) <= key((15*8)+7 downto 12*8) xor rconedKeyColumn;
    newKeyBuf((11*8)+7 downto 8*8) <= key((11*8)+7 downto 8*8) xor newKeyBuf((15*8)+7 downto 12*8);    
    newKeyBuf((7*8)+7 downto 4*8) <= key((7*8)+7 downto 4*8) xor newKeyBuf((11*8)+7 downto 8*8);    
    newKeyBuf((3*8)+7 downto 0*8) <= key((3*8)+7 downto 0*8) xor newKeyBuf((7*8)+7 downto 4*8);
    
    newKey <= newKeyBuf;
end behavioral;
