library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity Round is
    port (
        round : in unsigned(3 downto 0);
        state : in std_logic_vector(aes.kAesSize-1 downto 0);
        newState : out std_logic_vector(aes.kAesSize-1 downto 0);
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
end Round;

architecture rtl of Round is
    component SubBytes is
    port(
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
    end component;
    component ShiftRows is
    port(
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        
        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
    end component;
    component MixColumns is
    port (
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        
        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
    end component;
    component AddRoundKey is
        port(
            round : in unsigned(3 downto 0);
            state : in std_logic_vector(aes.kAesSize-1 downto 0);
            newState : out std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
        );
    end component;
    
    signal subbedBytes : std_logic_vector(aes.kAesSize-1 downto 0);
    signal shiftedRows : std_logic_vector(aes.kAesSize-1 downto 0);
    signal mixedColumns : std_logic_vector(aes.kAesSize-1 downto 0);
begin
    subBytesComp : SubBytes port map (
        input => state,
        output => subbedBytes
    );
    shiftRowsComp : ShiftRows port map (
        column0 => subbedBytes((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        column1 => subbedBytes((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        column2 => subbedBytes((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        column3 => subbedBytes((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn),
        newColumn0 => shiftedRows((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        newColumn1 => shiftedRows((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        newColumn2 => shiftedRows((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        newColumn3 => shiftedRows((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn)
    );
    mixColumnsComp : MixColumns port map (
        column0 => shiftedRows((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        column1 => shiftedRows((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        column2 => shiftedRows((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        column3 => shiftedRows((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn),
        newColumn0 => mixedColumns((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        newColumn1 => mixedColumns((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        newColumn2 => mixedColumns((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        newColumn3 => mixedColumns((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn)        
    );
    addRoundKeyComp : AddRoundKey port map (
        round => round,
        state => mixedColumns,
        newState => newState,
        key => key,
        newKey => newKey
    );
end rtl;
