library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SBox_tb is
end SBox_tb;

architecture tb of SBox_tb is
    component SBox is
    port(
        input : in std_logic_vector(7 downto 0);
        output : out std_logic_vector(7 downto 0)
    );
    end component;
    
    signal input : std_logic_vector(7 downto 0);
    signal output : std_logic_vector(7 downto 0);
begin
    comp: SBox port map (
        input => input,
        output => output
    );
    
    process
    begin
        for i in 0 to 255 loop
            input <= std_logic_vector(to_unsigned(i, input'length));
            wait for 10 ns;
        end loop; 
    end process;
end tb;
