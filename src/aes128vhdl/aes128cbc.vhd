library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity AES128_cbc is
    port (
        trigger : in std_logic;
        clk : in std_logic;
        initializationVector : in std_logic_vector(aes.kAesSize-1 downto 0);
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
        outputReady : out std_logic := '0'
    );
end AES128_cbc;

architecture behavioral of AES128_cbc is
    component AES128 is
        port (
            clk : in std_logic;
            reset : in std_logic;
            input : in std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            output : out std_logic_vector(aes.kAesSize-1 downto 0);
            outputReady : out std_logic := '0'
        );
    end component;
    
    signal firstBlock : std_logic := '1';
    signal cbcInput : std_logic_vector(aes.kAesSize-1 downto 0);
    signal aes128EcbOutput : std_logic_vector(aes.kAesSize-1 downto 0);
    signal outputLoopback : std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
    signal aes128EcbOutputReady : std_logic;
    signal outputReadyLoopback : std_logic := '0';
begin
    comp : AES128 port map (
        clk => clk,
        reset => trigger,
        input => cbcInput,
        key => key,
        output => aes128EcbOutput,
        outputReady => aes128EcbOutputReady
    ); 
    
    output <= outputLoopback;
    outputReady <= outputReadyLoopback;
    
    
    process (clk)
        variable outputReadyLoopbackVar : std_logic;
        variable outputLoopbackVar : std_logic_vector(outputLoopback'length-1 downto 0);
        variable first : std_logic;
    begin
        if (rising_edge(clk))
        then
            outputReadyLoopbackVar := outputReadyLoopback;
            outputLoopbackVar := outputLoopback;
            first := firstBlock;
            if (aes128EcbOutputReady = '1')
            then
                if (first = '0')
                then
                    outputLoopbackVar := aes128EcbOutput;
                    outputReadyLoopbackVar := '1';
                end if;
            end if;
            
            if (trigger = '1')
            then
                if (first = '1')
                then
                    cbcInput <= input xor initializationVector;
                    first := '0';
                elsif (outputReadyLoopbackVar = '1')
                then
                    cbcInput <= input xor outputLoopback;                
                end if;
                outputReadyLoopbackVar := '0';
            end if;
            outputReadyLoopback <= outputReadyLoopbackVar;
            outputLoopback <= outputLoopbackVar;
            firstBlock <= first;
        end if;
    end process;
end behavioral;
