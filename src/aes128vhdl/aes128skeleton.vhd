library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity Aes128Skeleton is
	generic (
		clk_divider : integer := 5000000
		);
	port (
		-- control interface
		clock				: in std_logic;
		reset				: in std_logic; -- controls functionality (sleep)
		busy				: out std_logic; -- done with entire calculation
				
		-- indicate new data or request
		rd		            : in std_logic;	-- request a variable
        wr 				    : in std_logic; 	-- request changing a variable
		
		-- data interface
		data_in			    : in unsigned(7 downto 0);
		address_in		    : in unsigned(15 downto 0);
        data_out			: out unsigned(7 downto 0)
	);
end Aes128Skeleton;

architecture behavioral of Aes128Skeleton is
    component AES128_cbc is
    port (
        trigger : in std_logic;
        clk : in std_logic;
        initializationVector : in std_logic_vector(aes.kAesSize-1 downto 0);
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
        outputReady : out std_logic := '0'
    );
    end component;
    
    signal trigger : std_logic;
    signal initializationVector : std_logic_vector(aes.kAesSize-1 downto 0);
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0) := std_logic_vector(to_unsigned(0, aes.kAesSize));
    signal outputReady : std_logic;
begin
    comp : AES128_cbc port map (
        trigger => trigger,
        clk => clock,
        initializationVector => initializationVector,
        input => input,
        key => key,
        output => output,
        outputReady => outputReady
    );

    busy <= not outputReady;
    
    process (clock)
        variable triggerVar : std_logic;
    begin
        if reset = '1'
        then
            initializationVector <= std_logic_vector(to_unsigned(0, initializationVector'length));
            key <= std_logic_vector(to_unsigned(0, initializationVector'length));
            input <= std_logic_vector(to_unsigned(0, initializationVector'length));
        else
            if rising_edge(clock)
            then
                triggerVar := '0';
                if wr = '0'
                then
                    case address_in is
                    when x"0000" =>
                        input(7 downto 0) <= std_logic_vector(data_in);
                    when x"0001" =>
                        input(15 downto 8) <= std_logic_vector(data_in);
                    when x"0002" => 
                        input(23 downto 16) <= std_logic_vector(data_in);
                    when x"0003" => 
                        input(31 downto 24) <= std_logic_vector(data_in);
                    when x"0004" => 
                        input(39 downto 32) <= std_logic_vector(data_in);
                    when x"0005" => 
                        input(47 downto 40) <= std_logic_vector(data_in);
                    when x"0006" => 
                        input(55 downto 48) <= std_logic_vector(data_in);
                    when x"0007" => 
                        input(63 downto 56) <= std_logic_vector(data_in);
                    when x"0008" => 
                        input(71 downto 64) <= std_logic_vector(data_in);
                    when x"0009" => 
                        input(79 downto 72) <= std_logic_vector(data_in);
                    when x"000a" => 
                        input(87 downto 80) <= std_logic_vector(data_in);
                    when x"000b" => 
                        input(95 downto 88) <= std_logic_vector(data_in);
                    when x"000c" => 
                        input(103 downto 96) <= std_logic_vector(data_in);
                    when x"000d" => 
                        input(111 downto 104) <= std_logic_vector(data_in);
                    when x"000e" => 
                        input(119 downto 112) <= std_logic_vector(data_in);
                    when x"000f" => 
                        input(127 downto 120) <= std_logic_vector(data_in);
                    when x"0010" =>
                        key(7 downto 0) <= std_logic_vector(data_in);
                    when x"0011" =>
                        key(15 downto 8) <= std_logic_vector(data_in);
                    when x"0012" => 
                        key(23 downto 16) <= std_logic_vector(data_in);
                    when x"0013" => 
                        key(31 downto 24) <= std_logic_vector(data_in);
                    when x"0014" => 
                        key(39 downto 32) <= std_logic_vector(data_in);
                    when x"0015" => 
                        key(47 downto 40) <= std_logic_vector(data_in);
                    when x"0016" => 
                        key(55 downto 48) <= std_logic_vector(data_in);
                    when x"0017" => 
                        key(63 downto 56) <= std_logic_vector(data_in);
                    when x"0018" => 
                        key(71 downto 64) <= std_logic_vector(data_in);
                    when x"0019" => 
                        key(79 downto 72) <= std_logic_vector(data_in);
                    when x"001a" => 
                        key(87 downto 80) <= std_logic_vector(data_in);
                    when x"001b" => 
                        key(95 downto 88) <= std_logic_vector(data_in);
                    when x"001c" => 
                        key(103 downto 96) <= std_logic_vector(data_in);
                    when x"001d" => 
                        key(111 downto 104) <= std_logic_vector(data_in);
                    when x"001e" => 
                        key(119 downto 112) <= std_logic_vector(data_in);
                    when x"001f" => 
                        key(127 downto 120) <= std_logic_vector(data_in);
                    when x"0020" => 
                        initializationVector(7 downto 0) <= std_logic_vector(data_in);
                    when x"0021" => 
                        initializationVector(15 downto 8) <= std_logic_vector(data_in);
                    when x"0022" => 
                        initializationVector(23 downto 16) <= std_logic_vector(data_in);
                    when x"0023" => 
                        initializationVector(31 downto 24) <= std_logic_vector(data_in);
                    when x"0024" => 
                        initializationVector(39 downto 32) <= std_logic_vector(data_in);
                    when x"0025" => 
                        initializationVector(47 downto 40) <= std_logic_vector(data_in);
                    when x"0026" => 
                        initializationVector(55 downto 48) <= std_logic_vector(data_in);
                    when x"0027" => 
                        initializationVector(63 downto 56) <= std_logic_vector(data_in);
                    when x"0028" => 
                        initializationVector(71 downto 64) <= std_logic_vector(data_in);
                    when x"0029" => 
                        initializationVector(79 downto 72) <= std_logic_vector(data_in);
                    when x"002a" => 
                        initializationVector(87 downto 80) <= std_logic_vector(data_in);
                    when x"002b" => 
                        initializationVector(95 downto 88) <= std_logic_vector(data_in);
                    when x"002c" => 
                        initializationVector(103 downto 96) <= std_logic_vector(data_in);
                    when x"002d" => 
                        initializationVector(111 downto 104) <= std_logic_vector(data_in);
                    when x"002e" => 
                        initializationVector(119 downto 112) <= std_logic_vector(data_in);
                    when x"002f" => 
                        initializationVector(127 downto 120) <= std_logic_vector(data_in);
                    when x"00ff" =>
                        triggerVar := '1';
                    when others => 
                    end case;
                elsif rd = '0'
                then
                    case address_in is
                    when x"0100" =>
                        data_out <= unsigned(output(7 downto 0));
                    when x"0101" =>
                        data_out <= unsigned(output(15 downto 8));
                    when x"0102" => 
                        data_out <= unsigned(output(23 downto 16));
                    when x"0103" => 
                        data_out <= unsigned(output(31 downto 24));
                    when x"0104" => 
                        data_out <= unsigned(output(39 downto 32));
                    when x"0105" => 
                        data_out <= unsigned(output(47 downto 40));
                    when x"0106" => 
                        data_out <= unsigned(output(55 downto 48));
                    when x"0107" => 
                        data_out <= unsigned(output(63 downto 56));
                    when x"0108" => 
                        data_out <= unsigned(output(71 downto 64));
                    when x"0109" => 
                        data_out <= unsigned(output(79 downto 72));
                    when x"010a" => 
                        data_out <= unsigned(output(87 downto 80));
                    when x"010b" => 
                        data_out <= unsigned(output(95 downto 88));
                    when x"010c" => 
                        data_out <= unsigned(output(103 downto 96));
                    when x"010d" => 
                        data_out <= unsigned(output(111 downto 104));
                    when x"010e" => 
                        data_out <= unsigned(output(119 downto 112));
                    when x"010f" => 
                        data_out <= unsigned(output(127 downto 120));
                    when x"01ff" =>
                        data_out <= to_unsigned(0, data_out'length);
                        data_out(0) <= outputReady;
                    when others =>
                    end case;
                end if;
                trigger <= triggerVar;
            end if;
        end if;
    end process;
end Behavioral;
