library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity GenerateNextKey_tb is
end GenerateNextKey_tb;

architecture tb of GenerateNextKey_tb is
    component GenerateNextKey is
        port (
            key : in std_logic_vector(127 downto 0);
            round : in unsigned(3 downto 0);
            newKey : out std_logic_vector(127 downto 0)        
        );
    end component;
    
    signal key : std_logic_vector(127 downto 0);
    signal round : unsigned(3 downto 0);
    signal newKey : std_logic_vector(127 downto 0);
begin
    comp : GenerateNextKey port map (
        key => key,
        round => round,
        newKey => newKey
    );

    process
    begin
        -- https://www.youtube.com/watch?v=gP4PqVGudtg
        key <= x"2b7e151628aed2a6abf7158809cf4f3c";
        for r in 1 to 10
        loop
            round <= to_unsigned(r, round'length);
            wait for 10 ns;
            key <= newKey;
        end loop;
    end process;
end tb;
