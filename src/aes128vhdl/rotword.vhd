library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity RotWord is
    port (
        input : in std_logic_vector(31 downto 0);
        output : out std_logic_vector(31 downto 0)
    );
end RotWord;

architecture rtl of RotWord is
begin    
    output(7 downto 0) <= input(31 downto 24);
    output(31 downto 8) <= input(23 downto 0);
end rtl;
