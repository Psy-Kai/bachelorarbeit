library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity ShiftRows_tb is
end shiftrows_tb;

architecture tb of ShiftRows_tb is
    component ShiftRows is
    port(
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        
        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
    end component;
    
    signal input : std_logic_vector(127 downto 0);
    signal column0 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal column1 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal column2 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal column3 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal newColumn0 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal newColumn1 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal newColumn2 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal newColumn3 : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
begin
    comp: ShiftRows port map (
        column0 => column0,
        column1 => column1,
        column2 => column2,
        column3 => column3,
        newColumn0 => newColumn0,
        newColumn1 => newColumn1,
        newColumn2 => newColumn2,
        newColumn3 => newColumn3
    );
    
    column0 <= input((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn);
    column1 <= input((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn);
    column2 <= input((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn);
    column3 <= input((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn);
    
    process
    begin
        for i in 0 to 15 loop
            input(127 downto 0) <= std_logic_vector(to_unsigned(0, input'length));
            input((i*8)+7 downto (i*8)) <= std_logic_vector(to_unsigned(1, 8));
            wait for 10 ns;
        end loop;
    end process;
end tb;
