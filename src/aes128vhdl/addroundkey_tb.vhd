library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity AddRoundKey_tb is
end AddRoundKey_tb;

architecture tb of AddRoundKey_tb is
    component AddRoundKey is
        port(
            round : in unsigned(3 downto 0);
            state : in std_logic_vector(aes.kAesSize-1 downto 0);
            newState : out std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
        );
    end component;
    
    signal round : unsigned(3 downto 0);
    signal state : std_logic_vector(aes.kAesSize-1 downto 0);
    signal newState : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal newKey : std_logic_vector(aes.kAesSize-1 downto 0);
begin
    comp : AddRoundKey port map (
        round => round,
        state => state,
        newState => newState,
        key => key,
        newKey => newKey
    );

    process
    begin
        round <= "0001";
        state <= x"3243f6a8885a308d313198a2e0370734"; -- expect x"193de3bea0f4e22b9ac68d2ae9f84808"
        key <= x"2b7e151628aed2a6abf7158809cf4f3c"; -- expect x"a0fafe1788542cb123a339392a6c7605"
        wait for 10ns;
    end process;
end tb;
