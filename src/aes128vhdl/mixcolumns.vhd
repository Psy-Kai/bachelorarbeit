library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.AES;

entity MixColumns is
    port (
        column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);

        newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
end MixColumns;

architecture rtl of MixColumns is
    component MixColumn is
    port ( 
        column : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
    end component;
begin
    mixColumn0 : MixColumn port map (
        column => column0,
        newColumn => newColumn0
    );
    mixColumn1 : MixColumn port map (
        column => column1,
        newColumn => newColumn1
    );
    mixColumn2 : MixColumn port map (
        column => column2,
        newColumn => newColumn2
    );
    mixColumn3 : MixColumn port map (
        column => column3,
        newColumn => newColumn3
    );
end rtl;
