library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity GenerateNextKey is
    port (
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        round : in unsigned(3 downto 0);
        newKey : out std_logic_vector(aes.kAesSize-1 downto 0)        
    );
end GenerateNextKey;

architecture rtl of GenerateNextKey is
    component RotWord is
        port (
            input : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            output : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
        );
    end component;
    component SBox is
    port(
        input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
        output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
    );
    end component;
    component Rcon is
        port (
            input : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            round : in unsigned(3 downto 0);
            output : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
        );
    end component;
    component XorKeys is
    port (
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        rconedKeyColumn : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
    end component;
    
    signal rotatedKeyColumn : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal substitutedKeyColumn : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal rconedKeyColumn : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
begin
    rotWordComp : RotWord port map (
        input => key((3*aes.kBitsPerByte)+7 downto 0*aes.kBitsPerByte),
        output => rotatedKeyColumn(aes.kBitsPerColumn-1 downto 0)
    );
    sboxes: for i in 0 to 3 generate
        box : SBox port map (
            input => rotatedKeyColumn((i*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto i*aes.kBitsPerByte),
            output => substitutedKeyColumn((i*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto i*aes.kBitsPerByte) 
        );
    end generate sboxes;
    rconComp : RCon port map (
        input => substitutedKeyColumn,
        round => round,
        output => rconedKeyColumn
    );
    xorKeysComp : XorKeys port map (
        key => key,
        rconedKeyColumn => rconedKeyColumn,
        newKey => newKey
    );
end rtl;
