library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity GaloisMultiplication_Mult2_tb is
end GaloisMultiplication_Mult2_tb;

architecture tb of GaloisMultiplication_Mult2_tb is
    component GaloisMultiplication_Mult2 is
        port (
            input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
            output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
        );
    end component;

    signal input : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal output : std_logic_vector(aes.kBitsPerByte-1 downto 0);
begin
    comp : GaloisMultiplication_Mult2 port map (
        input => input,
        output => output
    );

    process
    begin
        for i in 0 to 255
        loop
            input <= std_logic_vector(to_unsigned(i, input'length));
            wait for 10 ns;
        end loop;
    end process;
end tb;
