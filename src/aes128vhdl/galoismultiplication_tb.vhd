library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity GaloisMultiplication_tb is
end GaloisMultiplication_tb;

architecture tb of GaloisMultiplication_tb is
    component GaloisMultiplication is
        generic (
            kMultiplicand : integer        
        );
        port (
            input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
            output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
        );
    end component;
    
    signal input : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal output1 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal output2 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal output3 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal outputX : std_logic_vector(aes.kBitsPerByte-1 downto 0);
begin
    mult1 : GaloisMultiplication generic map (        
        kMultiplicand => 1
    )
    port map (
        input => input,
        output => output1 
    );
    mult2 : GaloisMultiplication generic map (        
        kMultiplicand => 2
    )
    port map (
        input => input,
        output => output2 
    );
    mult3 : GaloisMultiplication generic map (        
        kMultiplicand => 3
    )
    port map (
        input => input,
        output => output3 
    );
    multX : GaloisMultiplication generic map (        
        kMultiplicand => 4
    )
    port map (
        input => input,
        output => outputX 
    );

    process
    begin
        for i in 0 to 255
        loop
            input <= std_logic_vector(to_unsigned(i, aes.kBitsPerByte));
            wait for 10 ns;
        end loop;
    end process;
end tb;
