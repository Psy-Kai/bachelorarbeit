library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.AES;

entity AES128_tb is
end AES128_tb;

architecture tb of AES128_tb is
    component AES128 is
        port (
            clk : in std_logic;
            reset : in std_logic;
            input : in std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            output : out std_logic_vector(aes.kAesSize-1 downto 0);
            outputReady : out std_logic := '0'
        );
    end component;

    signal clk : std_logic := '0';
    signal reset : std_logic := '0';
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal key : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
    signal outputReady : std_logic;
begin
    comp : AES128 port map (
        clk => clk,
        reset => reset,
        input => input,
        key => key,
        output => output,
        outputReady => outputReady
    );
    
    input <= x"3243f6a8885a308d313198a2e0370734";
    key <= x"2b7e151628aed2a6abf7158809cf4f3c";
    
    process
    begin
        reset <= '1';
        clk <= '1';
        wait for 1ns;
        clk <= '0';
        wait for 1ns;
        reset <= '0';
        clk <= '1';
        wait for 1ns;
        clk <= '0';
        wait for 1ns;
        
        while (outputReady = '0')
        loop
            clk <= '1';
            wait for 1ns;
            clk <= '0';
            wait for 1ns;            
        end loop;
        clk <= '1';
        wait for 1ns;
        clk <= '0';
        wait for 1ns;    
    end process;
end tb;
