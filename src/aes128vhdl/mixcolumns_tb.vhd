library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.AES;

entity MixColumns_tb is
end MixColumns_tb;

architecture tb of MixColumns_tb is
    component MixColumns is
        port (
            column0 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            column1 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            column2 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            column3 : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            
            newColumn0 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            newColumn1 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            newColumn2 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            newColumn3 : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
        );
    end component;
    
    signal input : std_logic_vector(aes.kAesSize-1 downto 0);
    signal output : std_logic_vector(aes.kAesSize-1 downto 0);
begin
    comp : MixColumns port map (
        column0 => input((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        column1 => input((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        column2 => input((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        column3 => input((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn),
        newColumn0 => output((3*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 3*aes.kBitsPerColumn),
        newColumn1 => output((2*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 2*aes.kBitsPerColumn),
        newColumn2 => output((1*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 1*aes.kBitsPerColumn),
        newColumn3 => output((0*aes.kBitsPerColumn)+aes.kBitsPerColumn-1 downto 0*aes.kBitsPerColumn)
    );

    process
    begin
        input <= x"d4bf5d30e0b452aeb84111f11e2798e5"; -- expects x" 046681e5e0cb199a48f8d37a2806264c"
        wait for 10ns;
    end process;
end tb;