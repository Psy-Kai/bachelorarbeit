library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity RotWord_tb is
end RotWord_tb;

architecture tb of RotWord_tb is
    component RotWord is
        port (
            input : in std_logic_vector(31 downto 0);
            output : out std_logic_vector(31 downto 0)
        );
    end component;
    
    signal input : std_logic_vector(31 downto 0);
    signal output : std_logic_vector(31 downto 0);
begin
    comp : RotWord port map(
        input => input,
        output => output
    );
    
    process
    begin
        for bytePos in 0 to 3
        loop
            input((bytePos*8)+7 downto bytePos*8) <= std_logic_vector(to_unsigned(bytePos+1, 8));
        end loop;
        wait for 10 ns;
    end process;
end tb;
