library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity MixColumn is
    port (
        column : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
        newColumn : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
    );
end MixColumn;

architecture rtl of MixColumn is
    component GaloisMultiplication is
        generic (
            kMultiplicand : integer        
        );
        port (
            input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
            output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
        );
    end component;

    --  b0 = (a0 * 2) xor (a1 * 3) xor (a2 * 1) xor (a3 * 1)
    --  b1 = (a0 * 1) xor (a1 * 2) xor (a2 * 3) xor (a3 * 1)
    --  b2 = (a0 * 1) xor (a1 * 1) xor (a2 * 2) xor (a3 * 3)
    --  b3 = (a0 * 3) xor (a1 * 1) xor (a2 * 1) xor (a3 * 2)
    signal b0a0 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b0a1 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b1a1 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b1a2 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b2a2 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b2a3 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b3a3 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    signal b3a0 : std_logic_vector(aes.kBitsPerByte-1 downto 0);
    constant kColumnIndex0 : natural := 3;
    constant kColumnIndex1 : natural := 2;
    constant kColumnIndex2 : natural := 1;
    constant kColumnIndex3 : natural := 0;
begin
    b0a0Comp : GaloisMultiplication generic map (
        kMultiplicand => 2
    ) 
    port map (
        input => column((kColumnIndex0*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex0*aes.kBitsPerByte),
        output => b0a0
    );
    b0a1Comp : GaloisMultiplication generic map (
        kMultiplicand => 3
    ) 
    port map (
        input => column((kColumnIndex1*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex1*aes.kBitsPerByte),
        output => b0a1
    );
    b1a1Comp : GaloisMultiplication generic map (
        kMultiplicand => 2
    ) 
    port map (
        input => column((kColumnIndex1*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex1*aes.kBitsPerByte),
        output => b1a1
    );
    b1a2Comp : GaloisMultiplication generic map (
        kMultiplicand => 3
    ) 
    port map (
        input => column((kColumnIndex2*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex2*aes.kBitsPerByte),
        output => b1a2
    );
    b2a2Comp : GaloisMultiplication generic map (
        kMultiplicand => 2
    ) 
    port map (
        input => column((kColumnIndex2*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex2*aes.kBitsPerByte),
        output => b2a2
    );
    b2a3Comp : GaloisMultiplication generic map (
        kMultiplicand => 3
    ) 
    port map (
        input => column((kColumnIndex3*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex3*aes.kBitsPerByte),
        output => b2a3
    );
    b3a3Comp : GaloisMultiplication generic map (
        kMultiplicand => 2
    ) 
    port map (
        input => column((kColumnIndex3*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex3*aes.kBitsPerByte),
        output => b3a3
    );
    b3a0Comp : GaloisMultiplication generic map (
        kMultiplicand => 3
    ) 
    port map (
        input => column((kColumnIndex0*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex0*aes.kBitsPerByte),
        output => b3a0
    );
    
    newColumn((kColumnIndex0*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex0*aes.kBitsPerByte) <=
        b0a0 xor b0a1 xor 
        column((kColumnIndex2*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex2*aes.kBitsPerByte) xor 
        column((kColumnIndex3*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex3*aes.kBitsPerByte);
    newColumn((kColumnIndex1*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex1*aes.kBitsPerByte) <=
        column((kColumnIndex0*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex0*aes.kBitsPerByte) xor 
        b1a1 xor b1a2 xor
        column((kColumnIndex3*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex3*aes.kBitsPerByte);
    newColumn((kColumnIndex2*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex2*aes.kBitsPerByte) <=
        column((kColumnIndex0*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex0*aes.kBitsPerByte) xor 
        column((kColumnIndex1*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex1*aes.kBitsPerByte) xor 
        b2a2 xor b2a3;
    newColumn((kColumnIndex3*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex3*aes.kBitsPerByte) <= 
        b3a0 xor 
        column((kColumnIndex1*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex1*aes.kBitsPerByte) xor
        column((kColumnIndex2*aes.kBitsPerByte) + aes.kBitsPerByte-1 downto kColumnIndex2*aes.kBitsPerByte) xor
        b3a3;
end rtl;