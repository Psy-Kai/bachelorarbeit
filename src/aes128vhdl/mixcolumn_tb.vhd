library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity MixColumn_tb is
end MixColumn_tb;

architecture tb of MixColumn_tb is
    component MixColumn is
        port (
            column : in std_logic_vector(aes.kBitsPerColumn-1 downto 0);
            newColumn : out std_logic_vector(aes.kBitsPerColumn-1 downto 0)
        );
    end component;
    
    signal column : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
    signal newColumn : std_logic_vector(aes.kBitsPerColumn-1 downto 0);
begin
    comp : MixColumn port map (
        column => column,
        newColumn => newColumn
    );

    process
    begin
        column <= x"d4bf5d30"; -- expect x"046681e5"
        wait for 10 ns;
        column <= x"e0b452ae"; -- expect x"e0cb199a"
        wait for 10 ns;
        column <= x"b84111f1"; -- expect x"48f8d37a"
        wait for 10 ns;
        column <= x"1e2798e5"; -- expect x"2806264c"
        wait for 10 ns;
    end process;
end tb;
