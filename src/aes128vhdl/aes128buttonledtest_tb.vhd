library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity AES128ButtonLedTest_tb is
end AES128ButtonLedTest_tb;

architecture tb of AES128ButtonLedTest_tb is
    component AES128ButtonLedTest is
    generic (
        debounceClkCount : unsigned
    );
    port (
        clk : in std_logic;
        button : in std_logic;                      -- BTN3
        ledSuccess : out std_logic := '0';          -- LD1 green
        ledError : out std_logic := '0';            -- LD1 red
        ledOutputReady : out std_logic := '0';      -- LD0 green
        ledTrigger : out std_logic := '0';          -- LD0 blue
        ledTestOutput : out std_logic_vector(3 downto 0)     -- LD5  dwonto LD2
    );
    end component;

    signal clk : std_logic := '0';
    signal button : std_logic := '0';
    signal ledSuccess : std_logic;
    signal ledError : std_logic;
    signal ledOutputReady : std_logic;
    signal ledTrigger : std_logic;
    signal ledTestOutput : std_logic_vector(3 downto 0);
begin
    comp : AES128ButtonLedTest generic map (
        debounceClkCount => to_unsigned(1, 1)
    ) 
    port map (
        clk => clk,
        button => button,
        ledSuccess => ledSuccess,
        ledError => ledError,
        ledOutputReady => ledOutputReady,
        ledTrigger => ledTrigger,
        ledTestOutput => ledTestOutput
    );
    
    process
        variable outputReady : std_logic;
    begin
        outputReady := ledOutputReady;
        clk <= '0';
        button <= '0';
        
        for i in 0 to 100
        loop
            clk <= not clk;
            wait for 83ns;
        end loop;
                
        button <= '1';
        for i in 0 to 4
        loop
            clk <= not clk;
            wait for 83ns;
        end loop;
        button <= '0';
        for i in 0 to 4
        loop
            clk <= not clk;
            wait for 83ns;
        end loop;
    
        while (ledOutputReady = outputReady)
        loop
            clk <= not clk;
            wait for 83ns;
        end loop;
--        button <= '1';
--        wait for 1ns;
--        button <= '0';
--        wait for 10ns;
    end process;
end tb;
