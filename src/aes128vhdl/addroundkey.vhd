library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use WORK.AES;

entity AddRoundKey is
    port(
        round : in unsigned(3 downto 0);
        state : in std_logic_vector(aes.kAesSize-1 downto 0);
        newState : out std_logic_vector(aes.kAesSize-1 downto 0);
        key : in std_logic_vector(aes.kAesSize-1 downto 0);
        newKey : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
end AddRoundKey;

architecture rtl of AddRoundKey is
    component ApplyKey is
        port(
            input : in std_logic_vector(aes.kAesSize-1 downto 0);
            output : out std_logic_vector(aes.kAesSize-1 downto 0);
            key : in std_logic_vector(aes.kAesSize-1 downto 0)
        );
    end component;
    component GenerateNextKey is
        port (
            key : in std_logic_vector(aes.kAesSize-1 downto 0);
            round : in unsigned(3 downto 0);
            newKey : out std_logic_vector(aes.kAesSize-1 downto 0)        
        );
    end component;
begin
    applyKeyComp: ApplyKey port map (
        input => state,
        output => newState,
        key => key
    );
    generateNewKeyComp : GenerateNextKey port map (
        key => key,
        round => round,
        newKey => newKey
    );
end rtl;
