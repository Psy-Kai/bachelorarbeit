library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Rcon_tb is
end Rcon_tb;

architecture tb of Rcon_tb is
    component Rcon is
        port (
            input : in std_logic_vector(31 downto 0);
            round : in unsigned(3 downto 0);
            output : out std_logic_vector(31 downto 0)
        );
    end component;
    
    signal input : std_logic_vector(31 downto 0);
    signal round : unsigned(3 downto 0);
    signal output : std_logic_vector(31 downto 0);
begin
    comp : RCon port map (
        input => input,
        round => round,
        output => output
    );

    process
    begin
        for r in 1 to 10
        loop
            input <= x"44332211";
            round <= to_unsigned(r, round'length);
            wait for 10 ns;
        end loop;
    end process;
end tb;
