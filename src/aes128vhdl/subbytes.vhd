library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.AES;

entity SubBytes is
    port(
        input : in std_logic_vector(aes.kAesSize-1 downto 0);
        output : out std_logic_vector(aes.kAesSize-1 downto 0)
    );
end SubBytes;

architecture rtl of SubBytes is
    component SBox is
    port(
        input : in std_logic_vector(aes.kBitsPerByte-1 downto 0);
        output : out std_logic_vector(aes.kBitsPerByte-1 downto 0)
    );
    end component;
begin
    sboxes: for i in 0 to 15 generate
        box : SBox port map (
            input => input((i*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto i*aes.kBitsPerByte),
            output => output((i*aes.kBitsPerByte)+aes.kBitsPerByte-1 downto i*aes.kBitsPerByte) 
        );
    end generate sboxes;
end rtl;
