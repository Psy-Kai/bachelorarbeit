#include "aes128.h"

#include <assert.h>
#include <string.h>
#if defined(__AVR__)
#include "lib/debug/debug.h"
#include "lib/fpga/fpga.h"
#include "lib/multiboot/multiboot.h"
#include "lib/xmem/xmem.h"
#endif
#include "aes128def.h"

#define measurepin_white_high() (PORTC |= _BV(PC7))
#define measurepin_white_low() {(PORTC &= ~_BV(PC7)); _delay_ms(1);}

#define aes128_measurepin_high() //measurepin_white_high()
#define aes128_measurepin_low() //measurepin_white_low()

inline volatile uint8_t *memOffset(struct Aes128 *thiz, int offset)
{
    return (uint8_t*)thiz->userlogicOffset + offset;
}

inline volatile uint8_t state(struct Aes128 *thiz)
{
    return *memOffset(thiz, AES128_OUTPUT_STATE);
}

int aes128_configureFpga(struct Aes128 *thiz)
{
    (void)thiz;
#if defined(__AVR__)

    enableXmem();
    fpgaSoftReset();
    fpgaMultiboot(AES128_FPGA_ADDRESS);
    fpgaSetDoneReponse(FPGA_DONE_NOTHING);
    while (!fpgaMultibootComplete()) {}
    enableXmem();
    fpgaSoftReset();
    uint8_t id = fpgaGetHWFID();
    debugWriteHex8(id);
    if (id != AES128_FPGA_HWFID)
        return 1;
#endif
    return 1;
}

void aes128_initValues(struct Aes128 *thiz, struct Key *key, struct InitializationVector *iv)
{
    assert(key != NULL);
    assert(iv != NULL);

    memcpy(memOffset(thiz, AES128_INPUT_KEY), key->key, AES128_BLOCK_SIZE);
    memcpy(memOffset(thiz, AES128_INPUT_IV), iv->iv, AES128_BLOCK_SIZE);
}

int aes128_init(struct Aes128 *thiz, struct Key *key, struct InitializationVector *iv)
{
    if (!aes128_configureFpga(thiz))
        return 0;
#if !defined(__AVR__)
    (void)key;
    (void)iv;
    assert(0);
#else
    aes128_init2(thiz, USERLOGIC_OFFSET, key, iv);
#endif
    return 1;
}

void aes128_init2(struct Aes128 *thiz, uintptr_t offset, struct Key *key,
                  struct InitializationVector *iv)
{
    assert(thiz != NULL);
    thiz->userlogicOffset = offset;
    aes128_initValues(thiz, key, iv);
}

void aes128_encrypt(struct Aes128 *thiz, const uint8_t *data, uint8_t *result, size_t size)
{
    assert(thiz != NULL);
    assert(data != NULL);
    assert(result != NULL);

    struct Block input;
    input.aes128 = thiz;
    struct Block output;
    output.aes128 = thiz;

    for (size_t i = 0; i < size; i += AES128_BLOCK_SIZE) {
        input.count = (uint8_t)(AES128_BLOCK_SIZE < (size-i) ? AES128_BLOCK_SIZE : (size-i));
        memcpy(input.data, &data[i], input.count);
        if (input.count < AES128_BLOCK_SIZE)
            memset(&input.data[input.count], 0, AES128_BLOCK_SIZE-input.count);

        aes128_block_encrypt(&input, &output);
        memcpy(&result[i], output.data, AES128_BLOCK_SIZE);
    }
}

void aes128_block_init(struct Block *block, struct Aes128 *aes128)
{
    assert(block != NULL);

    block->count = 0;
    memset(block->data, 0, sizeof(block->data));
    block->aes128 = aes128;
}

void aes128_block_encrypt(struct Block *block, struct Block *result)
{
    assert(block != NULL);
    assert(result != NULL);

    /* pass data parameter to hardware function */
    aes128_measurepin_high();
    memcpy(memOffset(block->aes128, AES128_INPUT_DATA), block->data, AES128_BLOCK_SIZE);
    aes128_measurepin_low();

    /* trigger processing of hardware function */
    aes128_measurepin_high();
    *memOffset(block->aes128, AES128_INPUT_TRIGGER) = 1;
    aes128_measurepin_low();

    /* busy waiting until hardware-function is ready */
    aes128_measurepin_high();
    while (!(state(block->aes128) & AES128_STATE_BIT_READY)) {}
    aes128_measurepin_low();

    /* read result of hardware function */
    aes128_measurepin_high();
    memcpy(result->data, memOffset(block->aes128, AES128_OUTPUT_DATA), AES128_BLOCK_SIZE);
    aes128_measurepin_low();
}
